ARG IMG_TAG=4.2.0-jdk11
FROM atlassian/crowd:$IMG_TAG
RUN chown -R ${RUN_USER}:root               ${CROWD_HOME}/ \
    && chown -R ${RUN_USER}:root            ${CROWD_INSTALL_DIR}/apache-tomcat/logs \
    && chown -R ${RUN_USER}:root            ${CROWD_INSTALL_DIR}/apache-tomcat/webapps \
    && chown -R ${RUN_USER}:root            ${CROWD_INSTALL_DIR}/apache-tomcat/temp \
    && chown -R ${RUN_USER}:root            ${CROWD_INSTALL_DIR}/apache-tomcat/work \
    && chown -R ${RUN_USER}:root            ${CROWD_INSTALL_DIR}/apache-tomcat/conf \
    && chown -R ${RUN_USER}:root            ${CROWD_INSTALL_DIR}/database \
    && chmod -R 775                         ${CROWD_INSTALL_DIR}/apache-tomcat/logs \
    && chmod -R 775                         ${CROWD_INSTALL_DIR}/apache-tomcat/webapps \
    && chmod -R 775                         ${CROWD_INSTALL_DIR}/apache-tomcat/temp \
    && chmod -R 775                         ${CROWD_INSTALL_DIR}/apache-tomcat/work \
    && chmod -R 775                         ${CROWD_INSTALL_DIR}/apache-tomcat/conf \
    && chmod -R 775                         ${CROWD_INSTALL_DIR}/database
